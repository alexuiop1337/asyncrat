AsyncRAT Update 2020.11

1 - Use SHA256 for Client HWID
We are using SHA256 throughout the project already, for the sake of consistency we should use SHA256 for client identification. This method also allows for setting of client ID's of a specific length. Note: Truncated SHA256 is still more unique than MD5

2 - Refactored Self Delete Method
Currently the method works by writing a .batch script to disk, this method isn't very efficient and can be achieved by passing commands to CMD's startup parameters (/c) and a delay of 5 seconds (/t 5). We also don't have to worry about leaving a .bat script on the disk after the client is uninstalled

3 -  Windows DPI Remote Desktop Fix
Fixed position mapping between server side bitmap (remote desktop form) and client screen
Tested at
16:9 480p
16:9 720p
16:9 1080p 
18:9 4k





AsyncRAT is a Remote Access Tool (RAT) designed to remotely monitor and control other computers through a secure encrypted connection

Included projects
This project includes the following

    Plugin system to send and receive commands
    Access terminal for controlling clients
    Configurable client manageable via Terminal
    Log server recording all significant events

Features Include:

    Client screen viewer & recorder
    Client Antivirus & Integrity manager
    Client SFTP access including upload & download
    Client & Server chat window
    Client Dynamic DNS & Multi-Server support (Configurable)
    Client Password Recovery
    Client JIT compiler
    Client Keylogger
    Client Anti Analysis (Configurable)
    Server Controlled updates
    Client Antimalware Start-up
    Server Config Editor
    Server multiport receiver (Configurable)
    Server thumbnails
    Server binary builder (Configurable)
    Server obfuscator (Configurable)
    And much more!

Installation & Deployment

AsyncRAT requires the .Net Framework v4 (client) and v4.6+ (server) to run.

- to compile this project(s) visual studio 2019 or above to is required